import { Controller } from 'stimulus'

export default class extends Controller {
  connect() {}

  addIngredient() {
    $('#ingredients').append('<div><input type="text" name="ingredients[]"></div>')
  }

  addStep() {
    $('#steps').append('<div><textarea name="steps[]"></textarea></div>')
  }
  
}