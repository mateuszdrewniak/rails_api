class ProductController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :check_method, :check_params

  def multiply
    render json: { 'token': @hash['token'], 'product': @hash['a'] * @hash['b'] }
  end

  private

  def check_method
    render json: { 'status': 405 }, status: 405 unless request.post?
  end

  def check_params
    params.permit!
    @hash = params.require(:product).to_h
    render json: { 'status': 400 }, status: 400 if incorrect_params? || incorrect_values?
  end

  def incorrect_params?
    @hash.count != 3 || !@hash['token'].is_a?(Integer) || !@hash['a'].is_a?(Integer) || !@hash['b'].is_a?(Integer)
  end

  def incorrect_values?
    @hash['token'] <= 0 || @hash['a'] <= 0 || @hash['b'] <= 0
  end
end
