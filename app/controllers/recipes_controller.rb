class RecipesController < ApplicationController
  before_action :set_recipe, only: %i[show edit update destroy]
  skip_before_action :verify_authenticity_token, only: :api_create

  # GET /recipes
  # GET /recipes.json
  def index
    @recipes = Recipe.all
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
  end

  # GET /recipes/1/edit
  def edit
  end

  # POST /recipe/api/new
  def api_create
    recipe_params = Utils::RecipeParams.new(params)

    begin
      recipe_params.validate!
      recipe = Recipe.new(name: recipe_params.name, photo: recipe_params.photo)
      recipe.save!

      recipe_params.ingredients.each do |val|
        Ingredient.new(name: val, recipe_id: recipe.id).save!
      end

      recipe_params.steps.each do |val|
        Step.new(content: val, recipe_id: recipe.id).save!
      end

      render json: { id: recipe.id }
    rescue
      render json: { status: 422, error: 'Unprocessable Entity' }, status: 422
    end
  end

  # POST /recipes
  # POST /recipes.json
  def create
    begin
      @recipe = Recipe.new(recipe_params)
      @recipe.save!
      create_ingredients
      create_steps
      respond_to do |format|
        format.html { redirect_to alt_recipe_path(@recipe), notice: 'Recipe was successfully created.' }
        format.json { render :show, status: :created, location: @recipe }
      end
    rescue
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    begin
      update_ingredients
      update_steps
      @recipe.update!(recipe_params)
      respond_to do |format|
        format.html { redirect_to alt_recipe_path(@recipe), notice: 'Recipe was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipe }
      end
    rescue
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to recipes_url, notice: 'Recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def update_ingredients
    return unless params['ingredients']

    ingredients = @recipe.ingredients
    y = -1
    for y in 0..(ingredients.count - 1)
      if params['ingredients'][y].empty?
        ingredients[y].destroy
      elsif ingredients[y].name != params['ingredients'][y]
        ingredients[y].name = params['ingredients'][y]
        ingredients[y].save!
      end
    end

    for j in (y + 1)..(params['ingredients'].count - 1)
      unless params['ingredients'][j].empty?
        ingredient = Ingredient.new(name: params['ingredients'][j], recipe_id: @recipe.id)
        ingredient.save!
      end
    end
  end

  def update_steps
    return unless params['steps']

    steps = @recipe.steps
    y = -1
    for y in 0..(steps.count - 1)
      if params['steps'][y].empty?
        steps[y].destroy
      elsif steps[y].content != params['steps'][y]
        steps[y].content = params['steps'][y]
        steps[y].save!
      end
    end

    for j in (y + 1)..(params['steps'].count - 1)
      unless params['steps'][j].empty?
        step = Step.new(content: params['steps'][j], recipe_id: @recipe.id)
        step.save!
      end
    end
  end

  def create_ingredients
    return unless params['ingredients']

    for i in 0..(params['ingredients'].count - 1)
      unless params['ingredients'][i].empty?
        ingredient = Ingredient.new(name: params['ingredients'][i], recipe_id: @recipe.id)
        ingredient.save!
      end
    end
  end

  def create_steps
    return unless params['steps']

    for i in 0..(params['steps'].count - 1)
      unless params['steps'][i].empty?
        step = Step.new(content: params['steps'][i], recipe_id: @recipe.id)
        step.save!
      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_recipe
    @recipe = Recipe.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def recipe_params
    params.require(:recipe).permit(:name, :photo)
  end
end
