require 'json-schema'

module Utils
  class RecipeParams
    attr_accessor :name, :photo, :ingredients, :steps

    def initialize(params)
      @name = params['name']
      @photo = params['photo']
      @ingredients = params['ingredients']
      @steps = params['steps']
    end

    def validate!
      raise StandardError unless params_present?

      validate_schema!
    end

    def to_h
      {
        'name' => @name,
        'photo' => @photo,
        'ingredients' => @ingredients,
        'steps' => @steps
      }
    end

    private

    def params_present?
      @name && @photo && @ingredients && @steps
    end

    def recipe_schema
      {
        'type' => 'object',
        'required' => %w[name photo ingredients steps],
        'properties' => {
          'name' => { 'type' => 'string' },
          'photo' => { 'type' => 'string' },
          'ingredients' => { 'type' => 'array' },
          'steps' => { 'type' => 'array' }
        }
      }
    end

    def validate_schema!
      JSON::Validator.validate!(recipe_schema, to_h)
    end
  end
end
