Rails.application.routes.draw do
  match 'product', to: 'product#multiply', via: :all

  resources :recipes
  get 'recipe/new', to: 'recipes#new', as: :alt_new_recipe
  get 'recipe/:id', to: 'recipes#show', as: :alt_recipe
  get 'recipe/:id/edit', to: 'recipes#edit', as: :alt_edit_recipe
  post 'recipe/api/new', to: 'recipes#api_create'

end
